import React from "react";
import { Route, Switch } from "react-router-dom";
import VideosPage from './pages/VideosPage'
import PDFPage from './pages/PDFPage'


export default ({ childProps }) =>
<Switch>
    <Route path="/" exact component={VideosPage} />
    <Route path="/pdfs" exact component={PDFPage} />
</Switch>