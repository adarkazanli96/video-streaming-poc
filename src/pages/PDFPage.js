import React from 'react'
import axios from 'axios'


export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          documents : [],
          testUrl : ""
        };
      }
    
      async componentDidMount(){
        // fetch signed video URLs from lambda (which pulls urls from the database)
        await axios.get('https://p46gnv18m8.execute-api.us-east-1.amazonaws.com/dev', {
            params : {
                type : "document"
            }
        })
        .then((response) => {
          console.log('this is the response from the lambda!', response.data)
          this.setState({documents: response.data})
        })
    }

    renderDocuments(){
      return this.state.documents.map(url => {
        // encode the URLs
        let encodedUrl = encodeURIComponent(url);
        let iFrameUrl = 'https://docs.google.com/viewer?url=' + encodedUrl + '&embedded=true';
        return(
          <iframe key = {url} src = {iFrameUrl} width= "600px" height = "500px" frameBorder="0"></iframe>
        )
      })
    }

    handleChange = (event) => {
      this.setState({testUrl:  event.target.value})
    }

    handleSubmit = (event) => {
      event.preventDefault()
      let url = this.state.testUrl
      let encodedUrl = encodeURIComponent(url);
      let iFrameUrl = 'https://docs.google.com/gview?url=' + encodedUrl + '&embedded=true';
      let generated = <iframe src = {iFrameUrl} width= "600px" height = "500px" frameBorder="0"></iframe>
      this.setState({generated : generated})
  }

    render(){
        
        return(<div>
          <form onSubmit = {this.handleSubmit}>
            Test a URL: <input onChange = {this.handleChange} value = {this.state.testUrl}/>
          </form>
          {this.state.generated}
          <hr/>
          {this.renderDocuments()}
  
        </div>)
    }
}