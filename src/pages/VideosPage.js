import React from 'react'
import axios from 'axios'

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            videos : []
        };
    }

    async componentDidMount(){
        // fetch signed video URLs from lambda (which pulls urls from the database)
        await axios.get('https://p46gnv18m8.execute-api.us-east-1.amazonaws.com/dev', {
            params : {
                type : "video"
            }
        })
        .then((response) => {
          console.log('this is the response from the lambda!', response.data)
          this.setState({videos: response.data})
        })
    }

    renderVideos(){
        return this.state.videos.map(url => {
            return (<video key = {url} controls>
            <source src = {url} type="video/mp4"/>
            </video>)
        })
    }

    render(){
        return(<div>
            {this.renderVideos()}
        </div>)
    }
}