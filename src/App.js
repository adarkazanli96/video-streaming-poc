import React from 'react';
import './App.css';
import Routes from './Routes'
import { Link, withRouter } from "react-router-dom";

class App extends React.Component {

  render(){

  return (<div className="App">
    <Link to="/">Videos</Link><span>{" "}</span>
    <Link to="/pdfs">PDFs</Link>
    <Routes/>
  </div>);
}
}

export default withRouter(App);
